<?php 

function AO_AA04_RAW_TWAMP_EID_HOURLY_main() {
	
	global $G_DBCONN_MAIN; 

	//server sumber
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ";
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

	//delete data 15 hari kebelakang
	$get_date = "SELECT DISTINCT date_id FROM t_raw_twamp_eid_hourly ORDER BY date_id DESC";  
	$get_query_date = mysqli_query($G_DBCONN_MAIN,$get_date);
	 
	$a = 0;  
	foreach($get_query_date as $item) {
		if($a >= 15) {
			$query  = "DELETE FROM t_raw_twamp_eid_hourly where date_id = '".$item['date_id']."'"; 
			$delete = mysqli_query($G_DBCONN_MAIN,$query);
		}
	$a++;
	} 

	//PROSES 
	//get data 3 hari kebelakang
	for($x=1; $x<=3; $x++) {
			
			//date parameter
			$date = date('Y-m-d', strtotime('-'.$x.' day', strtotime(date('Y-m-d')))); 
			
			//proses          
			$get_data_query = "SELECT * FROM RAW_TWAMP_EID_HOURLY WHERE date_id = '".$date."'";         
			$result = $conn_sumber->query($get_data_query);                
			if(!empty($result)) { 
 				
					//delete data terlebih dahulu berdasarkan tanggal yang didapat, agar tidak double didatabase
					//tidak memakai REPLACE, karna narik data apa adanya dari database sumber
					$delete_data_query = "DELETE FROM t_raw_twamp_eid_hourly WHERE date_id = '".$date."'";     
					$result_delete = $G_DBCONN_MAIN->query($delete_data_query);   

					while($fault = mysqli_fetch_array($result))
					{   
							$date_id			= str_replace(array("'"), "", $fault['date_id']);  
							$time_id			= str_replace(array("'"), "", $fault['time_id']); 
							$reg_name		        = str_replace(array("'"), "", $fault['reg_name']); 
							$site_id	                = str_replace(array("'"), "", $fault['site_id']); 
							$site_name      		= str_replace(array("'"), "", $fault['site_name']); 
							$ip_address_source	        = str_replace(array("'"), "", $fault['ip_address_source']);  
							$ip_address_target		= str_replace(array("'"), "", $fault['ip_address_target']); 
							$max_packetloss			= str_replace(array("'"), "", $fault['max_packetloss']); 
							$avg_packetloss			= str_replace(array("'"), "", $fault['avg_packetloss']); 
							$min_packetloss			= str_replace(array("'"), "", $fault['min_packetloss']);  
							$packetloss_sd		        = str_replace(array("'"), "", $fault['packetloss_sd']);
							$packetloss_ds		        = str_replace(array("'"), "", $fault['packetloss_ds']); 
							$pkt_duplicate_sd	        = str_replace(array("'"), "", $fault['pkt_duplicate_sd']); 
							$pkt_duplicate_ds           	= str_replace(array("'"), "", $fault['pkt_duplicate_ds']);  
							$pkt_reorder_sd	                = str_replace(array("'"), "", $fault['pkt_reorder_sd']); 
							$pkt_reorder_ds           	= str_replace(array("'"), "", $fault['pkt_reorder_ds']); 
							$avg_jitter		        = str_replace(array("'"), "", $fault['avg_jitter']); 
							$max_jitter	            	= str_replace(array("'"), "", $fault['max_jitter']);  
							$min_jitter		        = str_replace(array("'"), "", $fault['min_jitter']); 
							$avg_latency			= str_replace(array("'"), "", $fault['avg_latency']); 
							$max_latency			= str_replace(array("'"), "", $fault['max_latency']); 
							$min_latency			= str_replace(array("'"), "", $fault['min_latency']);  
							$latency_pctl95		    	= str_replace(array("'"), "", $fault['latency_pctl95']); 
							$latency_pctl98		    	= str_replace(array("'"), "", $fault['latency_pctl98']); 
							$latency_pctl99			= str_replace(array("'"), "", $fault['latency_pctl99']);   
							$avg_jitter_sd		        = str_replace(array("'"), "", $fault['avg_jitter_sd']);  
							$max_jitter_sd		        = str_replace(array("'"), "", $fault['max_jitter_sd']); 
							$min_jitter_sd      		= str_replace(array("'"), "", $fault['min_jitter_sd']);   
							$jitter_sd_pctl95	    	= str_replace(array("'"), "", $fault['jitter_sd_pctl95']); 
							$jitter_sd_pctl98		= str_replace(array("'"), "", $fault['jitter_sd_pctl98']); 
							$jitter_sd_pctl99	        = str_replace(array("'"), "", $fault['jitter_sd_pctl99']);  
							$avg_jitter_ds		        = str_replace(array("'"), "", $fault['avg_jitter_ds']); 
							$max_jitter_ds			= str_replace(array("'"), "", $fault['max_jitter_ds']); 
							$min_jitter_ds			= str_replace(array("'"), "", $fault['min_jitter_ds']); 
							$jitter_ds_pctl95	        = str_replace(array("'"), "", $fault['jitter_ds_pctl95']);  
							$jitter_ds_pctl98		= str_replace(array("'"), "", $fault['jitter_ds_pctl98']); 
							$jitter_ds_pctl99		= str_replace(array("'"), "", $fault['jitter_ds_pctl99']); 
							$packet_sd			= str_replace(array("'"), "", $fault['packet_sd']);   
							$packet_ds		        = str_replace(array("'"), "", $fault['packet_ds']);  
							$lost_periods_sd		= str_replace(array("'"), "", $fault['lost_periods_sd']); 
							$lost_periods_ds      		= str_replace(array("'"), "", $fault['lost_periods_ds']);   
							$loss_packet_sd	    	        = str_replace(array("'"), "", $fault['loss_packet_sd']); 
							$loss_packet_ds		    	= str_replace(array("'"), "", $fault['loss_packet_ds']); 
							$pckt_reorder_sd		= str_replace(array("'"), "", $fault['pckt_reorder_sd']); 
							$pckt_reorder_ds	        = str_replace(array("'"), "", $fault['pckt_reorder_ds']);     
							
							$list_row	= "('$date_id','$time_id','$reg_name','$site_id','$site_name','$ip_address_source','$ip_address_target','$max_packetloss','$avg_packetloss','$min_packetloss','$packetloss_sd','$packetloss_ds','$pkt_duplicate_sd','$pkt_duplicate_ds','$pkt_reorder_sd','$pkt_reorder_ds','$avg_jitter','$max_jitter','$min_jitter','$avg_latency','$max_latency','$min_latency','$latency_pctl95','$latency_pctl98','$latency_pctl99','$avg_jitter_sd','$max_jitter_sd','$min_jitter_sd','$jitter_sd_pctl95','$jitter_sd_pctl98','$jitter_sd_pctl99','$avg_jitter_ds','$max_jitter_ds','$min_jitter_ds','$jitter_ds_pctl95','$jitter_ds_pctl98','$jitter_ds_pctl99','$packet_sd','$packet_ds','$lost_periods_sd','$lost_periods_ds','$loss_packet_sd','$loss_packet_ds','$pckt_reorder_sd','$pckt_reorder_ds')";         
							
							$insert_row = "INSERT INTO t_raw_twamp_eid_hourly (date_id,time_id,reg_name,site_id,site_name,ip_address_source,ip_address_target,max_packetloss,avg_packetloss,min_packetloss,packetloss_sd,packetloss_ds,pkt_duplicate_sd,pkt_duplicate_ds,pkt_reorder_sd,pkt_reorder_ds,avg_jitter,max_jitter,min_jitter,avg_latency,max_latency,min_latency,latency_pctl95,latency_pctl98,latency_pctl99,avg_jitter_sd,max_jitter_sd,min_jitter_sd,jitter_sd_pctl95,jitter_sd_pctl98,jitter_sd_pctl99,avg_jitter_ds,max_jitter_ds,min_jitter_ds,jitter_ds_pctl95,jitter_ds_pctl98,jitter_ds_pctl99,packet_sd,packet_ds,lost_periods_sd,lost_periods_ds,loss_packet_sd,loss_packet_ds,pckt_reorder_sd,pckt_reorder_ds) values $list_row;";          
							// print_r($insert_row); die();   
							$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
					}
			}        
	}

} 
?>