<?php 

function AO_AA02_RAW_IPPM_DAILY_main() {

	global $G_DBCONN_MAIN; 

	//server sumber 
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ";
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

	//delete data 15 hari kebelakang
	$get_date = "SELECT DISTINCT date_id FROM t_raw_ippm_daily ORDER BY date_id DESC";
	$get_query_date = mysqli_query($G_DBCONN_MAIN,$get_date);  
	 
	$a = 0; 
	foreach($get_query_date as $item) {
		if($a >= 15) {
			$query  = "DELETE FROM t_raw_ippm_daily where date_id = '".$item['date_id']."'"; 
			$delete = mysqli_query($G_DBCONN_MAIN,$query); 
		}
	$a++;
	}
  
	//PROSES
	//ambil data 3 hari kebelakang 
	for($x=1; $x<=3; $x++) {     
    
		//date parameter
		$date = date('Y-m-d', strtotime('-'.$x.' day', strtotime(date('Y-m-d'))));      
		
		$get_data_query = "SELECT * FROM RAW_IPPM_DAILY WHERE date_id = '".$date."'";          
		$result = $conn_sumber->query($get_data_query);                
		
		if(!empty($result)) { 
			
			//delete data terlebih dahulu berdasarkan tanggal yang didapat, agar tidak double didatabase
			//tidak memakai REPLACE, karna narik data apa adanya dari database sumber
			$delete_data_query = "DELETE FROM t_raw_ippm_daily WHERE date_id = '".$date."'";    
			$result_delete = $G_DBCONN_MAIN->query($delete_data_query);   
			 
			while($fault = mysqli_fetch_array($result))
			{
				
				if($fault['reg_numb'] == 1){
					$reg_name = '01-SUMBAGUT';
				}else if($fault['reg_numb'] == 2){
					$reg_name = '02-SUMBAGSEL';
				}else if($fault['reg_numb'] == 10){
					$reg_name = '10-SUMBAGTENG';
				}else if($fault['reg_numb'] == 3){
					$reg_name = '03-JABOTABEK';
				}else if($fault['reg_numb'] == 4){
					$reg_name = '04-JAWA BARAT'; 
				}else if($fault['reg_numb'] == 5){
					$reg_name = '05-JAWA TENGAH';
				}else if($fault['reg_numb'] == 6){
					$reg_name = '06-JAWA TIMUR';
				}else if($fault['reg_numb'] == 7){ 
					$reg_name = '07-BALI NUSRA'; 
				}else if($fault['reg_numb'] == 8){
					$reg_name = '08-KALIMANTAN';
				}else if($fault['reg_numb'] == 9){
					$reg_name = '09-SULAWESI';
				}else if($fault['reg_numb'] == 11){
					$reg_name = '11-PUMA';
				}
	
				$reg_name						= str_replace(array('"'), '', $reg_name); 
				$date_id						= str_replace(array('"'), '', $fault['date_id']); 
				$rnc_name						= str_replace(array('"'), '', $fault['rnc_name']);
				$ani				    	    = str_replace(array('"'), '', $fault['ani']); 
				$path_id	                    = str_replace(array('"'), '', $fault['path_id']);
				$ani	                        = str_replace(array('"'), '', $fault['ani']);
				$vendor_id					    = str_replace(array('"'), '', $fault['vendor_id']);
				$max_pl						    = str_replace(array('"'), '', $fault['max_pl']);  
				$count_max_pl					= str_replace(array('"'), '', $fault['count_max_pl']);
				$avg_pl				            = str_replace(array('"'), '', $fault['avg_pl']);
				$count_avg_pl					= str_replace(array('"'), '', $fault['count_avg_pl']);
				$flag_avg_daily				    = str_replace(array('"'), '', $fault['flag_avg_daily']);
				$flag_avg2_daily				= str_replace(array('"'), '', $fault['flag_avg2_daily']);
				$count_error_pl					= str_replace(array('"'), '', $fault['count_error_pl']);
				$count_lat40				    = str_replace(array('"'), '', $fault['cou nt_lat40']);
				$count_lat						= str_replace(array('"'), '', $fault['count_lat']);
				$flag_avg_lat				    = str_replace(array('"'), '', $fault['flag_avg_lat']);
				$flag_avg2_lat					= str_replace(array('"'), '', $fault['flag_avg2_lat']);
				$min_latency			    	= str_replace(array('"'), '', $fault['min_latency']);
				$avg_latency			        = str_replace(array('"'), '', $fault['avg_latency']);
				$max_latency		            = str_replace(array('"'), '', $fault['max_latency']); 
				$avg_jitter			            = str_replace(array('"'), '', $fault['avg_jitter']);
				$iub_drop				    	= str_replace(array('"'), '', $fault['iub_drop']);
				$crc_error				    	= str_replace(array('"'), '', $fault['crc_error']);
				$discards				        = str_replace(array('"'), '', $fault['discards']);
				$count_iub_drop		    		= str_replace(array('"'), '', $fault['count_iub_drop']);
				$count_iub_drop2		        = str_replace(array('"'), '', $fault['count_iub_drop2']);
				$flag_daily_iub_drop			= str_replace(array('"'), '', $fault['flag_daily_iub_drop']);
				$flag_daily_iub_drop2			= str_replace(array('"'), '', $fault['flag_daily_iub_drop2']);
				$flag_daily2_iub_drop		    = str_replace(array('"'), '', $fault['flag_daily2_iub_drop']);
				$flag_daily2_iub_drop2			= str_replace(array('"'), '', $fault['flag_daily2_iub_drop2']);
				$count_crc_error		    	= str_replace(array('"'), '', $fault['count_crc_error']);
				$flag_daily_crc					= str_replace(array('"'), '', $fault['flag_daily_crc']);
				$flag_daily_crc2				= str_replace(array('"'), '', $fault['flag_daily_crc2']);
				$count_discards		 	        = str_replace(array('"'), '', $fault['count_discards']);
				$flag_daily_discrads			= str_replace(array('"'), '', $fault['flag_daily_discrads']);
				$flag_daily_discrads2		    = str_replace(array('"'), '', $fault['flag_daily_discrads2']);
				$count_jitter		            = str_replace(array('"'), '', $fault['count_jitter']);
				$flag_jitter		    	    = str_replace(array('"'), '', $fault['flag_jitter']);
				$sum_iub_drop_num				= str_replace(array('"'), '', $fault['sum_iub_drop_num']);
				$avg_iub_drop_num		    	= str_replace(array('"'), '', $fault['avg_iub_drop_num']);
				$max_iub_drop_num				= str_replace(array('"'), '', $fault['max_iub_drop_num']); 
				$count_iub_drop_num		        = str_replace(array('"'), '', $fault['count_iub_drop_num']);
				$flag_daily_iub_drop_num	   	= str_replace(array('"'), '', $fault['flag_daily_iub_drop_num']);
				$status_pl_daily			    = str_replace(array('"'), '', $fault['status_pl_daily']);
				$status_lat_daily		        = str_replace(array('"'), '', $fault['status_lat_daily']);
				$status_jitt_daily				= str_replace(array('"'), '', $fault['status_jitt_daily']);
				$metro							= str_replace(array('"'), '', $fault['metro']);
				$input_date                     = date('Y-m-d H:i:s'); 
				
				$list_row	= "(\"$reg_name\",\"$date_id\",\"$rnc_name\",\"$ani\",\"$path_id\",\"$vendor_id\",\"$max_pl\",\"$count_max_pl\",\"$avg_pl\",\"$count_avg_pl\",\"$flag_avg_daily\",\"$flag_avg2_daily\",\"$count_error_pl\",\"$count_lat40\",\"$count_lat\",\"$flag_avg_lat\",\"$flag_avg2_lat\",\"$min_latency\",\"$avg_latency\",\"$max_latency\",\"$avg_jitter\",\"$iub_drop\",\"$crc_error\",\"$discards\",\"$count_iub_drop\",\"$count_iub_drop2\",\"$flag_daily_iub_drop\",\"$flag_daily_iub_drop2\",\"$flag_daily2_iub_drop\",\"$flag_daily2_iub_drop2\",\"$count_crc_error\",\"$flag_daily_crc\",\"$flag_daily_crc2\",\"$count_discards\",\"$flag_daily_discrads\",\"$flag_daily_discrads2\",\"$count_jitter\",\"$flag_jitter\",\"$sum_iub_drop_num\",\"$avg_iub_drop_num\",\"$max_iub_drop_num\",\"$count_iub_drop_num\",\"$flag_daily_iub_drop_num\",\"$status_pl_daily\",\"$status_lat_daily\",\"$status_jitt_daily\",\"$metro\",\"$input_date\")"; 
				
				$insert_row = "INSERT INTO t_raw_ippm_daily  
				(`reg_name`,`date_id`,`rnc_name`,`ani`,`path_id`,`vendor_id`,`max_pl`,`count_max_pl`,`avg_pl`,`count_avg_pl`,`flag_avg_daily`,`flag_avg2_daily`,`count_error_pl`,`count_lat40`,`count_lat`,`flag_avg_lat`,`flag_avg2_lat`,`min_latency`,`avg_latency`,`max_latency`,`avg_jitter`,`iub_drop`,`crc_error`,`discards`,`count_iub_drop`,`count_iub_drop2`,`flag_daily_iub_drop`,`flag_daily_iub_drop2`,`flag_daily2_iub_drop`,`flag_daily2_iub_drop2`,`count_crc_error`,`flag_daily_crc`,`flag_daily_crc2`,`count_discards`,`flag_daily_discrads`,`flag_daily_discrads2`,`count_jitter`,`flag_jitter`,`sum_iub_drop_num`,`avg_iub_drop_num`,`max_iub_drop_num`,`count_iub_drop_num`,`flag_daily_iub_drop_num`,`status_pl_daily`,`status_lat_daily`,`status_jitt_daily`,`metro`,`input_date`) values $list_row;";    
				
				$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
			}
		}
	}

} 
?>