<?php 

function AO_AA03_RAW_TWAMP_DAILY_main() {

	global $G_DBCONN_MAIN; 
	
	//server sumber 
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ";
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

	//delete data 15 hari kebelakang
	$get_date = "SELECT DISTINCT date_id FROM t_raw_twamp_daily ORDER BY date_id DESC";
	$get_query_date = mysqli_query($G_DBCONN_MAIN,$get_date);  
	
	$a = 0; 
	foreach($get_query_date as $item) { 
		if($a >= 15) {
			$query  = "DELETE FROM t_raw_twamp_daily where date_id = '".$item['date_id']."'"; 
			$delete = mysqli_query($G_DBCONN_MAIN,$query);
		}
	$a++;
	} 

	//PROSES 
	//get data 3 hari kebelakang
	for($x=1; $x<=3; $x++) {      
    
		//date parameter
		$date = date('Y-m-d', strtotime('-'.$x.' day', strtotime(date('Y-m-d'))));   
		
		//proses      
		$get_data_query = "SELECT * FROM RAW_TWAMP_DAILY WHERE date_id = '".$date."'";         
		$result = $conn_sumber->query($get_data_query);                   
		if(!empty($result)) { 

			//delete data terlebih dahulu berdasarkan tanggal yang didapat, agar tidak double didatabase
			//tidak memakai REPLACE, karna narik data apa adanya dari database sumber
			$delete_data_query = "DELETE FROM t_raw_twamp_daily WHERE date_id = '".$date."'";    
			$result_delete = $G_DBCONN_MAIN->query($delete_data_query);  
	
			while($fault = mysqli_fetch_array($result))
			{ 
				$reg_name						= str_replace(array('"'), '', $fault['reg_name']); 
				$date_id						= str_replace(array('"'), '', $fault['date_id']);
				$probe_name				    	= str_replace(array('"'), '', $fault['probe_name']);
				$site_id	                    = str_replace(array('"'), '', $fault['site_id']); 
				$site_name	                    = str_replace(array('"'), '', $fault['site_name']);
				$ip_address_source			    = str_replace(array('"'), '', $fault['ip_address_source']);
				$ip_address_target				= str_replace(array('"'), '', $fault['ip_address_target']);   
				$vendor					        = str_replace(array('"'), '', $fault['vendor']);
				$vvip_status		            = str_replace(array('"'), '', $fault['vvip_status']);
				$max_pl					        = str_replace(array('"'), '', $fault['max_pl']);
				$count_max_pl				    = str_replace(array('"'), '', $fault['count_max_pl']);
				$flag_max_daily				    = str_replace(array('"'), '', $fault['flag_max_daily']);
				$flag_max2_daily				= str_replace(array('"'), '', $fault['flag_max2_daily']);
				$avg_pl				            = str_replace(array('"'), '', $fault['avg_pl']);
				$count_avg_pl					= str_replace(array('"'), '', $fault['count_avg_pl']);
				$flag_avg_daily				    = str_replace(array('"'), '', $fault['flag_avg_daily']);
				$flag_avg2_daily				= str_replace(array('"'), '', $fault['flag_avg2_daily']);
				$count_lat	    		    	= str_replace(array('"'), '', $fault['count_lat']);
				$flag_lat		    	        = str_replace(array('"'), '', $fault['flag_lat']);
				$flag_lat2		                = str_replace(array('"'), '', $fault['flag_lat2']); 
				$min_latency		            = str_replace(array('"'), '', $fault['min_latency']);
				$avg_latency			    	= str_replace(array('"'), '', $fault['avg_latency']);
				$max_latency			    	= str_replace(array('"'), '', $fault['max_latency']);
				$avg_latency95				    = str_replace(array('"'), '', $fault['avg_latency95']);
				$avg_jitter		    		    = str_replace(array('"'), '', $fault['avg_jitter']);
				$count_jitter		            = str_replace(array('"'), '', $fault['count_jitter']);
				$count_error_pl			        = str_replace(array('"'), '', $fault['count_error_pl']);
				$daily_flag_error		    	= str_replace(array('"'), '', $fault['daily_flag_error']);
				$count_lat20		            = str_replace(array('"'), '', $fault['count_lat20']);
				$count_error_lat		    	= str_replace(array('"'), '', $fault['count_error_lat']);
				$status_pl_daily		    	= str_replace(array('"'), '', $fault['status_pl_daily']);
				$status_lat_daily				= str_replace(array('"'), '', $fault['status_lat_daily']);
				$status_jitt_daily				= str_replace(array('"'), '', $fault['status_jitt_daily']);
				$input_date                     = date('Y-m-d H:i:s'); 
				
				$list_row	= "(\"$reg_name\",\"$date_id\",\"$probe_name\",\"$site_id\",\"$site_name\",\"$ip_address_source\",\"$ip_address_target\",\"$vendor\",\"$vvip_status\",\"$max_pl\",\"$count_max_pl\",\"$flag_max_daily\",\"$flag_max2_daily\",\"$avg_pl\",\"$count_avg_pl\",\"$flag_avg_daily\",\"$flag_avg2_daily\",\"$count_lat\",\"$flag_lat\",\"$flag_lat2\",\"$min_latency\",\"$avg_latency\",\"$max_latency\",\"$avg_latency95\",\"$avg_jitter\",\"$count_jitter\",\"$count_error_pl\",\"$daily_flag_error\",\"$count_lat20\",\"$count_error_lat\",\"$status_pl_daily\",\"$status_lat_daily\",\"$status_jitt_daily\",\"$input_date\")"; 
				
				$insert_row = "INSERT INTO t_raw_twamp_daily   
				(`reg_name`,`date_id`,`probe_name`,`site_id`,`site_name`,`ip_address_source`,`ip_address_target`,`vendor`,`vvip_status`,`max_pl`,`count_max_pl`,`flag_max_daily`,`flag_max2_daily`,`avg_pl`,`count_avg_pl`,`flag_avg_daily`,`flag_avg2_daily`,`count_lat`,`flag_lat`,`flag_lat2`,`min_latency`,`avg_latency`,`max_latency`,`avg_latency95`,`avg_jitter`,`count_jitter`,`count_error_pl`,`daily_flag_error`,`count_lat20`,`count_error_lat`,`status_pl_daily`,`status_lat_daily`,`status_jitt_daily`,`input_date`) values $list_row;";  
				
				$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
			}
		}
	}

} 
?>