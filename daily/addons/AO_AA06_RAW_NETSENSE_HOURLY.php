<?php 

function AO_AA06_RAW_NETSENSE_HOURLY_main() {

	global $G_DBCONN_MAIN; 

	//server sumber
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ";
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

	//delete data 15 hari kebelakang
	$get_date = "SELECT DISTINCT date_id FROM t_raw_twamp_netsense_hourly ORDER BY date_id DESC"; 
	$get_query_date = mysqli_query($G_DBCONN_MAIN,$get_date); 

	$a = 0;   
	foreach($get_query_date as $item) {
		if($a >= 15) {
			$query  = "DELETE FROM t_raw_twamp_netsense_hourly where date_id = '".$item['date_id']."'"; 
			$delete = mysqli_query($G_DBCONN_MAIN,$query);
		}
	$a++;
	} 

	//PROSES 
	//get data 3 hari kebelakang
	for($x=1; $x<=3; $x++) {     
			
			//date parameter
			$date = date('Y-m-d', strtotime('-'.$x.' day', strtotime(date('Y-m-d'))));   
	
			//proses  
			$get_data_query = "SELECT * FROM RAW_TWAMP_NETSENSE_HOURLY WHERE date_id = '".$date."'";         
			$result = $conn_sumber->query($get_data_query);                
			if(!empty($result)) { 

					//delete data terlebih dahulu berdasarkan tanggal yang didapat, agar tidak double didatabase
					//tidak memakai REPLACE, karna narik data apa adanya dari database sumber
					$delete_data_query = "DELETE FROM t_raw_twamp_netsense_hourly WHERE date_id = '".$date."'";    
					$result_delete = $G_DBCONN_MAIN->query($delete_data_query);  

					while($fault = mysqli_fetch_array($result))
					{ 
							$date_id			= str_replace(array("'"), "", $fault['date_id']);  
							$time_id			= str_replace(array("'"), "", $fault['time_id']); 
							$reg_name		        = str_replace(array("'"), "", $fault['reg_name']); 
							$probeName	                = str_replace(array("'"), "", $fault['probeName']); 
							$ip_address_probe      		= str_replace(array("'"), "", $fault['ip_address_probe']); 
							$interface_name			= str_replace(array("'"), "", $fault['interface_name']);  
							$targetName		        = str_replace(array("'"), "", $fault['targetName']); 
							$ip_address_target		= str_replace(array("'"), "", $fault['ip_address_target']); 
							$avg_packetloss			= str_replace(array("'"), "", $fault['avg_packetloss']); 
							$max_packetloss			= str_replace(array("'"), "", $fault['max_packetloss']); 
							$min_packetloss			= str_replace(array("'"), "", $fault['min_packetloss']);  
							$avg_packetloss_sd		= str_replace(array("'"), "", $fault['avg_packetloss_sd']);
							$avg_packetloss_ds		= str_replace(array("'"), "", $fault['avg_packetloss_ds']); 
							$avg_latency			= str_replace(array("'"), "", $fault['avg_latency']); 
							$avg_jitter		    	= str_replace(array("'"), "", $fault['avg_jitter']);  
							$max_latency		        = str_replace(array("'"), "", $fault['max_latency']); 
							$max_jitter	            	= str_replace(array("'"), "", $fault['max_jitter']);  
							$packetloss		        = str_replace(array("'"), "", $fault['packetloss']); 
							$min_latency			= str_replace(array("'"), "", $fault['min_latency']); 
							$min_jitter			= str_replace(array("'"), "", $fault['min_jitter']); 
							$latency_sd			= str_replace(array("'"), "", $fault['latency_sd']);  
							$max_latency_sd		    	= str_replace(array("'"), "", $fault['max_latency_sd']); 
							$min_latency_sd		    	= str_replace(array("'"), "", $fault['min_latency_sd']); 
							$latency_ds			= str_replace(array("'"), "", $fault['latency_ds']);   
							$max_latency_ds		        = str_replace(array("'"), "", $fault['max_latency_ds']);  
							$min_latency_ds		        = str_replace(array("'"), "", $fault['min_latency_ds']); 
							$jitter_sd      		= str_replace(array("'"), "", $fault['jitter_sd']);   
							$jitter_ds	    	        = str_replace(array("'"), "", $fault['jitter_ds']);  
							
							$list_row	= "('$date_id','$time_id','$reg_name','$probeName','$ip_address_probe','$interface_name','$targetName','$ip_address_target','$avg_packetloss','$max_packetloss','$min_packetloss','$avg_packetloss_sd','$avg_packetloss_ds','$avg_latency','$avg_jitter','$max_latency','$max_jitter','$packetloss','$min_latency','$min_jitter','$latency_sd','$max_latency_sd','$min_latency_sd','$latency_ds','$max_latency_ds','$min_latency_ds','$jitter_sd','$jitter_ds')";         
							
							$insert_row = "INSERT INTO t_raw_twamp_netsense_hourly (date_id,time_id,reg_name,probeName,ip_address_probe,interface_name,targetName,ip_address_target,avg_packetloss,max_packetloss,min_packetloss,avg_packetloss_sd,avg_packetloss_ds,avg_latency,avg_jitter,max_latency,max_jitter,packetloss,min_latency,min_jitter,latency_sd,max_latency_sd,min_latency_sd,latency_ds,max_latency_ds,min_latency_ds,jitter_sd,jitter_ds) values $list_row;";          
							// print_r($insert_row); die(); 
							$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
					}
			}
	} 
} 
?>