<?php 

function AO_AA06_TRANSPORT_HEATMAP_SUMMARY_4G_main() {

	global $G_DBCONN_MAIN;  

	//proses
	$get_data_query = "SELECT a.weeknum, a.date_id, a.reg_name, a.site_id, a.site_name, a.category_avg21, a.category_latency, a.transport_type, b.long, b.lat
	FROM t_report_lte_weekly a 
	LEFT JOIN t_ref_ani_lte b ON a.site_id = b.site_id
	WHERE a.weeknum = (SELECT week FROM t_week_update) 
	AND YEAR(a.date_id) = YEAR(CURDATE())
	GROUP BY a.site_id, a.site_name";    
	$result = $G_DBCONN_MAIN->query($get_data_query);  

	if(!empty($result)){

	$delete_data_query = "DELETE FROM t_lte_transport_heatmap_summary WHERE weeknum = (SELECT week FROM t_week_update)";   
	$result_delete = $G_DBCONN_MAIN->query($delete_data_query); 

	while($fault = mysqli_fetch_array($result))
	{ 
	$weeknum            = str_replace(array('"'), '', $fault['weeknum']);  
	$date_id	        = str_replace(array('"'), '', $fault['date_id']);
	$reg_name	        = str_replace(array('"'), '', $fault['reg_name']);
	$site_id		    = str_replace(array('"'), '', $fault['site_id']);
	$site_name		    = str_replace(array('"'), '', $fault['site_name']);
	$category_avg21	    = str_replace(array('"'), '', $fault['category_avg21']);
	$category_latency   = str_replace(array('"'), '', $fault['category_latency']);
	$transport_type     = str_replace(array('"'), '', $fault['transport_type']);
	$longitude	        = str_replace(array('"'), '', $fault['long']);  
	$latitude		    = str_replace(array('"'), '', $fault['lat']);  
	$created_at         = date('Y-m-d H:i:s');

	$list_row               = "(\"$weeknum\",\"$date_id\",\"$reg_name\",\"$site_id\",\"$site_name\",\"$category_avg21\",\"$category_latency\",\"$transport_type\",\"$longitude\",\"$latitude\",\"$created_at\")"; 
	$insert_row             = "INSERT INTO t_lte_transport_heatmap_summary (`weeknum`,`date_id`,`reg_name`,`site_id`,`site_name`,`category_avg21`,`category_latency`,`transport_type`,`longitude`,`latitude`,`created_at`) values $list_row;";  
	$pushh                  = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
	}
	}

} 
?>