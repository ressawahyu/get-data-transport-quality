<?php 

function AO_AA07_PERFORMANCE_REPORT_main() {

		global $G_DBCONN_MAIN; 
		
		//proses          
		$get_data = "SELECT a.weeknum, a.reg_name, a.site_name2 AS site_id, b.site_name AS site_name_3g, c.site_name AS site_name_4g, a.longi, a.lat, b.category_avg21 AS status_3g, c.category_avg21 AS status_4g
		FROM t_ref_ani a
		JOIN (SELECT weeknum, date_id, site_id, site_name, category_avg21 FROM t_report_ippm_inp_weekly WHERE weeknum = (SELECT week FROM t_week_update) AND date_id = (SELECT MAX(date_id) FROM t_report_ippm_inp_weekly WHERE weeknum = (SELECT week FROM t_week_update))) b ON a.site_name2 = b.site_id
		JOIN (SELECT weeknum, date_id, site_id, site_name, category_avg21 FROM t_report_lte_weekly WHERE weeknum = (SELECT week FROM t_week_update) AND date_id = (SELECT MAX(date_id) FROM t_report_lte_weekly WHERE weeknum = (SELECT week FROM t_week_update))) c ON a.site_name2 = c.site_id
		WHERE a.weeknum = (SELECT week FROM t_week_update)
		AND (b.category_avg21 = 'CLEAR' OR b.category_avg21 = 'CONSECUTIVE')   
		AND (c.category_avg21 = 'CLEAR' OR c.category_avg21 = 'CONSECUTIVE') 
		";         
		$result = $G_DBCONN_MAIN->query($get_data) or die(mysqli_error($G_DBCONN_MAIN)); 

		if(!empty($result)) { 
		//delete row 
		$delete_data_query = "DELETE FROM t_performance_report WHERE weeknum = (SELECT week FROM t_week_update)";   
		$result_delete = $G_DBCONN_MAIN->query($delete_data_query);

		while($fault = mysqli_fetch_array($result))
		{
		$weeknum						= str_replace(array('"'), '', $fault['weeknum']); 
		$reg_name						= str_replace(array('"'), '', $fault['reg_name']); 
		$site_id				    	= str_replace(array('"'), '', $fault['site_id']);
		$site_name_3g					= str_replace(array('"'), '', $fault['site_name_3g']);
		$site_name_4g			    	= str_replace(array('"'), '', $fault['site_name_4g']);         
		$longi	                        = str_replace(array('"'), '', $fault['longi']);
		$lat	                        = str_replace(array('"'), '', $fault['lat']);  
		$status_3g  					= str_replace(array('"'), '', $fault['status_3g']);
		$status_4g  					= str_replace(array('"'), '', $fault['status_4g']);
	
		if($fault['status_3g'] == 'CLEAR' && $fault['status_4g'] == 'CLEAR') {
			$final_status = 'GREEN';
		} else if ($fault['status_3g'] == 'CONSECUTIVE' && $fault['status_4g'] == 'CONSECUTIVE') {
			$final_status = 'RED';
		} else { 
			$final_status = 'YELLOW';
		}

		$list_row	= "(\"$weeknum\",\"$reg_name\",\"$site_id\",\"$site_name_3g\",\"$site_name_4g\",\"$longi\",\"$lat\",\"$status_3g\",\"$status_4g\",\"$final_status\")";
		
		//insert
		$insert_row = "INSERT INTO t_performance_report 
			(`weeknum`,`reg_name`,`site_id`,`site_name_3g`,`site_name_4g`,`longi`,`lat`,`status_3g`,`status_4g`,`final_status`) values $list_row;";  

		$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
		}
	}
}    

?>