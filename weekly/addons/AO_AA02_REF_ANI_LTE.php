<?php 

function AO_AA02_REF_ANI_LTE_main() {

	global $G_DBCONN_MAIN;  

	//server sumber
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ";
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

	//proses get data		   
	$get_data_query = "SELECT * FROM REF_ANI_LTE WHERE weeknum = (SELECT max(weeknum) from REF_ANI_LTE)";
	$result = $conn_sumber->query($get_data_query);                
	
	if(!empty($result)) { 
		
		//delete week yang sudah melewati 1 bulan terakhir
		$delete_data_query = "DELETE FROM t_ref_ani WHERE weeknum <= (SELECT max(weeknum)-4 from t_ref_ani)";   
		$result_delete = $G_DBCONN_MAIN->query($delete_data_query); 
		
		$i = 0; 
		while($fault = mysqli_fetch_array($result))
		{  
			$regional			= str_replace(array('"'), '', $fault['regional']);  
			$reg_name			= str_replace(array('"'), '', $fault['reg_name']); 
			$system		          	= str_replace(array('"'), '', $fault['system']); 
			$site_id	                = str_replace(array('"'), '', $fault['site_id']); 
			$site_name      		= str_replace(array('"'), '', $fault['site_name']); 
			$ne_id			        = str_replace(array('"'), '', $fault['ne_id']);  
			$vendor				= str_replace(array('"'), '', $fault['vendor']); 
			$connected_router		= str_replace(array('"'), '', $fault['connected_router']); 
			$ip_address			= str_replace(array('"'), '', $fault['ip_address']); 
			$transport			= str_replace(array('"'), '', $fault['transport']); 
			$transport_type			= str_replace(array('"'), '', $fault['transport_type']);  
			$transport_mode			= str_replace(array('"'), '', $fault['transport_mode']); 
			$bw_actual			= str_replace(array('"'), '', $fault['bw_actual']); 
			$vlan_u				= str_replace(array('"'), '', $fault['vlan_u']); 
			$vlan_c			    	= str_replace(array('"'), '', $fault['vlan_c']); 
			$long			        = str_replace(array('"'), '', $fault['long']); 
			$lat		            	= str_replace(array('"'), '', $fault['lat']);  
			$desa			        = str_replace(array('"'), '', $fault['desa']); 
			$kabupaten			= str_replace(array('"'), '', $fault['kabupaten']); 
			$kecamatan			= str_replace(array('"'), '', $fault['kecamatan']);  
			$province			= str_replace(array('"'), '', $fault['province']);  
			$rtpo		    		= str_replace(array('"'), '', $fault['rtpo']); 
			$mac_address		    	= str_replace(array('"'), '', $fault['mac_address']); 
			$cluster_name			= str_replace(array('"'), '', $fault['cluster_name']); 
			$truecon_cluster		= str_replace(array('"'), '', $fault['truecon_cluster']); 
			$config_4g		        = str_replace(array('"'), '', $fault['config_4g']); 
			$destination_router		= str_replace(array('"'), '', $fault['destination_router']);  
			$treg		    	        = str_replace(array('"'), '', $fault['treg']);  
			$weeknum			= str_replace(array('"'), '', $fault['weeknum']);
			$metro			= str_replace(array('"'), '', $fault['metro']);		
			
			$list_row	= "(\"$regional\",\"$reg_name\",\"$system\",\"$site_id\",\"$site_name\",\"$ne_id\",\"$vendor\",\"$connected_router\",\"$ip_address\",\"$transport\",\"$transport_type\",\"$transport_mode\",\"$bw_actual\",\"$vlan_u\",\"$vlan_c\",\"$long\",\"$lat\",\"$desa\",\"$kabupaten\",\"$kecamatan\",\"$province\",\"$rtpo\",\"$mac_address\",\"$cluster_name\",\"$truecon_cluster\",\"$config_4g\",\"$destination_router\",\"$treg\",\"$weeknum\",\"$metro\")";
			
			if($i == 0) {  
				//sebelum data di insert, hapus dulu data berdasarkan week yang di get, agar tidak double
				//tidak pakai REPLACE dikarenakan narik apa adanya dari server, meminimalisir kesalahan penarikan data
				$delete_data_query_this_week = "DELETE FROM t_ref_ani_lte WHERE weeknum = $weeknum";  
				$result_delete_this_week = $G_DBCONN_MAIN->query($delete_data_query_this_week);
			}

			$insert_row = "INSERT INTO t_ref_ani_lte (`regional`,`reg_name`,`sistem`,`site_id`,`site_name`,`ne_id`,`vendor`,`connected_router`,`ip_address`,`transport`,`transport_type`,`transport_mode`,`bw_actual`,`vlan_u`,`vlan_c`,`long`,`lat`,`desa`,`kabupaten`,`kecamatan`,`province`,`rtpo`,`mac_address`,`cluster_name`,`truecon_cluster`,`config_4g`,`destination_router`,`treg`,`weeknum`,`metro`) values $list_row;";  
			
			$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
		}
		$i++;
	}
 
} 
?>