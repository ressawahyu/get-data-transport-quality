<?php 

function AO_AA04_REPORT_LTE_WEEKLY_main() {
	
	global $G_DBCONN_MAIN; 
 	
	//server sumber
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ_REPORT"; 
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

	for($x=0; $x<=7; $x++) { 

	//proses   
	$get_data_query = "SELECT * FROM REPORT_LTE_WEEKLY WHERE weeknum = (SELECT wcur FROM V_WEEKLY_CURRENT)-".$x." AND date_id=(SELECT MAX(date_id) FROM REPORT_LTE_WEEKLY WHERE weeknum = (SELECT wcur FROM V_WEEKLY_CURRENT)-".$x.")";     

	$result = $conn_sumber->query($get_data_query);     
	

	if(!empty($result)) {

		$delete_data_query = "DELETE FROM t_report_lte_weekly WHERE weeknum = (SELECT week FROM t_week_update)-".$x."";   
		$result_delete = $G_DBCONN_MAIN->query($delete_data_query);     

		while($fault = mysqli_fetch_array($result))
		{   
			$weeknum						= str_replace(array('"'), '', $fault['weeknum']); 
			$date_id						= str_replace(array('"'), '', $fault['date_id']);
			$reg_name				    	= str_replace(array('"'), '', $fault['reg_name']);
			$site_id	                    = str_replace(array('"'), '', $fault['site_id']);
			$site_name	                    = str_replace(array('"'), '', $fault['site_name']); 
			$vendor					    	= str_replace(array('"'), '', $fault['vendor']);
			$transport						= str_replace(array('"'), '', $fault['transport']);
			$transport_type					= str_replace(array('"'), '', $fault['transport_type']);
			$max_pl							= str_replace(array('"'), '', $fault['max_pl']);
			$counter_max_pl					= str_replace(array('"'), '', $fault['counter_max_pl']);
			$avg_pl							= str_replace(array('"'), '', $fault['avg_pl']);
			$counter_avg_pl					= str_replace(array('"'), '', $fault['counter_avg_pl']);
			$category_max35					= str_replace(array('"'), '', $fault['category_max35']);
			$category_avg21					= str_replace(array('"'), '', $fault['category_avg21']);
			$distribution_max			    = str_replace(array('"'), '', $fault['distribution_max']);
			$distribution_avg				= str_replace(array('"'), '', $fault['distribution_avg']);
			$count_error_pl			    	= str_replace(array('"'), '', $fault['count_error_pl']);
			$count_error_lat			    = str_replace(array('"'), '', $fault['count_error_lat']);
			$count_lat						= str_replace(array('"'), '', $fault['count_lat']);
			$count_lat20					= str_replace(array('"'), '', $fault['count_lat20']);
			$latency						= str_replace(array('"'), '', $fault['latency']);
			$max_latency					= str_replace(array('"'), '', $fault['max_latency']);
			$category_latency				= str_replace(array('"'), '', $fault['category_latency']);
			$category_latency20		    	= str_replace(array('"'), '', $fault['category_latency20']);
			$distribution_latency			= str_replace(array('"'), '', $fault['distribution_latency']);
			$jitter						    = str_replace(array('"'), '', $fault['jitter']);
			$count_jitter				    = str_replace(array('"'), '', $fault['count_jitter']);
			$category_jitter				= str_replace(array('"'), '', $fault['category_jitter']);
			$distribution_jitter			= str_replace(array('"'), '', $fault['distribution_jitter']);
			$red_transport_category			= str_replace(array('"'), '', $fault['red_transport_category']);
			$avg_dl_iubusage		    	= str_replace(array('"'), '', $fault['avg_dl_iubusage']);
			$max_dl_iubusage				= str_replace(array('"'), '', $fault['max_dl_iubusage']);
			$idx_dl_iubusage				= str_replace(array('"'), '', $fault['idx_dl_iubusage']);
			$status_iubusage		    	= str_replace(array('"'), '', $fault['status_iubusage']);
			$bw_actual			            = str_replace(array('"'), '', $fault['bw_actual']);
			$vlan_u				            = str_replace(array('"'), '', $fault['vlan_u']);
			$vlan_c			                = str_replace(array('"'), '', $fault['vlan_c']);
			$kabupaten		            	= str_replace(array('"'), '', $fault['kabupaten']);
			$rtpo					        = str_replace(array('"'), '', $fault['rtpo']);
			$cluster_name			    	= str_replace(array('"'), '', $fault['cluster_name']);
			$trueconex_cluster		    	= str_replace(array('"'), '', $fault['trueconex_cluster']);
			$connected_router			    = str_replace(array('"'), '', $fault['connected_router']);
			$config_4g				    	= str_replace(array('"'), '', $fault['config_4g']); 
			
			$list_row	= "(\"$weeknum\",\"$date_id\",\"$reg_name\",\"$site_id\",\"$site_name\",\"$vendor\",\"$transport\",\"$transport_type\",\"$max_pl\",\"$counter_max_pl\",\"$avg_pl\",\"$counter_avg_pl\",\"$category_max35\",\"$category_avg21\",\"$distribution_max\",\"$distribution_avg\",\"$count_error_pl\",\"$count_error_lat\",\"$count_lat\",\"$count_lat20\",\"$latency\",\"$max_latency\",\"$category_latency\",\"$category_latency20\",\"$distribution_latency\",\"$jitter\",\"$count_jitter\",\"$category_jitter\",\"$distribution_jitter\",\"$red_transport_category\",\"$avg_dl_iubusage\",\"$max_dl_iubusage\",\"$idx_dl_iubusage\",\"$status_iubusage\",\"$bw_actual\",\"$vlan_u\",\"$vlan_c\",\"$kabupaten\",\"$rtpo\",\"$cluster_name\",\"$trueconex_cluster\",\"$connected_router\",\"$config_4g\")";
			
			$insert_row = "INSERT INTO t_report_lte_weekly 
			(`weeknum`,`date_id`,`reg_name`,`site_id`,`site_name`,`vendor`,`transport`,`transport_type`,`max_pl`,`counter_max_pl`,`avg_pl`,`counter_avg_pl`,`category_max35`,`category_avg21`,`distribution_max`,`distribution_avg`,`count_error_pl`,`count_error_lat`,`count_lat`,`count_lat20`,`latency`,`max_latency`,`category_latency`,`category_latency20`,`distribution_latency`,`jitter`,`count_jitter`,`category_jitter`,`distribution_jitter`,`red_transport_category`,`avg_dl_iubusage`,`max_dl_iubusage`,`idx_dl_iubusage`,`status_iubusage`,`bw_actual`,`vlan_u`,`vlan_c`,`kabupaten`,`rtpo`,`cluster_name`,`trueconex_cluster`,`connected_router`,`config_4g`) 
			values $list_row;";  
			
			$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
		}
	} 
	}

} 
?>