<?php 

function AO_AA03_REPORT_IPPM_INP_WEEKLY_main() {

	global $G_DBCONN_MAIN; 
		
	//server sumber 
	$server_sumber      = "10.54.18.122:3306";
	$username_sumber    = "transport"; 
	$password_sumber    = "Transport#2019"; 
	$database_sumber    = "TNQ_REPORT"; 
	$conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);
 
	//get data 3 week terakhir
	for($x=0; $x<=3; $x++) { 
 
	//proses get data
	$get_data_query = "SELECT * FROM REPORT_IPPM_INP_WEEKLY WHERE weeknum = (SELECT wcur FROM V_WEEKLY_CURRENT)-".$x." AND date_id=(SELECT MAX(date_id) FROM REPORT_IPPM_INP_WEEKLY WHERE weeknum = (SELECT wcur FROM V_WEEKLY_CURRENT)-".$x.")";  
	$result = $conn_sumber->query($get_data_query);  

	if(!empty($result)) { 
	
	//delete 
	$delete_data_query = "DELETE FROM t_report_ippm_inp_weekly WHERE weeknum = (SELECT week FROM t_week_update)-".$x."";
	$result_delete = $G_DBCONN_MAIN->query($delete_data_query);  

	while($fault = mysqli_fetch_array($result))
	{ 
		$weeknum						= str_replace(array('"'), '', $fault['weeknum']); 
		$date_id						= str_replace(array('"'), '', $fault['date_id']);
		$reg_name				    	= str_replace(array('"'), '', $fault['reg_name']);
		$rnc_name	                    = str_replace(array('"'), '', $fault['rnc_name']);
		$ani	                        = str_replace(array('"'), '', $fault['ani']);
		$vendor					    	= str_replace(array('"'), '', $fault['vendor']);
		$site_id						= str_replace(array('"'), '', $fault['site_id']); 
		$site_name						= str_replace(array('"'), '', $fault['site_name']);
		$transport						= str_replace(array('"'), '', $fault['transport']);
		$transport_type					= str_replace(array('"'), '', $fault['transport_type']);
		$max_pl							= str_replace(array('"'), '', $fault['max_pl']);
		$counter_max					= str_replace(array('"'), '', $fault['counter_max']);
		$avg_pl							= str_replace(array('"'), '', $fault['avg_pl']);
		$counter_avg					= str_replace(array('"'), '', $fault['counter_avg']);
		$category_max35					= str_replace(array('"'), '', $fault['category_max35']);
		$category_avg21					= str_replace(array('"'), '', $fault['category_avg21']);  
		$distribution_max			    = str_replace(array('"'), '', $fault['distribution_max']); 
		$distribution_avg				= str_replace(array('"'), '', $fault['distribution_avg']);
		$count_error_pl			    	= str_replace(array('"'), '', $fault['count_error_pl']);
		$count_lat						= str_replace(array('"'), '', $fault['count_lat']);
		$latency						= str_replace(array('"'), '', $fault['latency']);
		$max_latency					= str_replace(array('"'), '', $fault['max_latency']);
		$category_latency				= str_replace(array('"'), '', $fault['category_latency']);
		$distribution_latency			= str_replace(array('"'), '', $fault['distribution_latency']);
		$jitter						    = str_replace(array('"'), '', $fault['jitter']);
		$category_jitter				= str_replace(array('"'), '', $fault['category_jitter']);
		$distribution_jitter			= str_replace(array('"'), '', $fault['distribution_jitter']);
		$count_iub_drop					= str_replace(array('"'), '', $fault['count_iub_drop']);
		$count_iub_drop01		    	= str_replace(array('"'), '', $fault['count_iub_drop01']);
		$iub_drop					    = str_replace(array('"'), '', $fault['iub_drop']);
		$category_iub_drop				= str_replace(array('"'), '', $fault['category_iub_drop']);
		$category_iub_drop01		 	= str_replace(array('"'), '', $fault['category_iub_drop01']);
		$count_crc			            = str_replace(array('"'), '', $fault['count_crc']); 
		$crc				            = str_replace(array('"'), '', $fault['crc']); 
		$category_crc			        = str_replace(array('"'), '', $fault['category_crc']);
		$count_discards			    	= str_replace(array('"'), '', $fault['count_discards']);
		$discards				    	= str_replace(array('"'), '', $fault['discards']);
		$category_discards		    	= str_replace(array('"'), '', $fault['category_discards']);
		$vlan_id				        = str_replace(array('"'), '', $fault['vlan_id']);
		$kabupaten		            	= str_replace(array('"'), '', $fault['kabupaten']);
		$rtpo					        = str_replace(array('"'), '', $fault['rtpo']);
		$cluster_name			    	= str_replace(array('"'), '', $fault['cluster_name']);
		$trueconex_cluster		    	= str_replace(array('"'), '', $fault['trueconex_cluster']);
		$config_3g				    	= str_replace(array('"'), '', $fault['config_3g']); 
		$count_availability			    = str_replace(array('"'), '', $fault['count_availability']);
		$availability_status	        = str_replace(array('"'), '', $fault['availability_status']);
		$sales_region			    	= str_replace(array('"'), '', $fault['sales_region']);
		$treg			                = str_replace(array('"'), '', $fault['treg']);
		$connected_router				= str_replace(array('"'), '', $fault['connected_router']);
		
		$list_row	= "(\"$weeknum\",\"$date_id\",\"$reg_name\",\"$rnc_name\",\"$ani\",\"$vendor\",\"$site_id\",\"$site_name\",\"$transport\",\"$transport_type\",\"$max_pl\",\"$counter_max\",\"$avg_pl\",\"$counter_avg\",\"$category_max35\",\"$category_avg21\",\"$distribution_max\",\"$distribution_avg\",\"$count_error_pl\",\"$count_lat\",\"$latency\",\"$max_latency\",\"$category_latency\",\"$distribution_latency\",\"$jitter\",\"$category_jitter\",\"$distribution_jitter\",\"$count_iub_drop\",\"$count_iub_drop01\",\"$iub_drop\",\"$category_iub_drop\",\"$category_iub_drop01\",\"$count_crc\",\"$crc\",\"$category_crc\",\"$count_discards\",\"$discards\",\"$category_discards\",\"$vlan_id\",\"$kabupaten\",\"$rtpo\",\"$cluster_name\",\"$trueconex_cluster\",\"$config_3g\",\"$count_availability\",\"$availability_status\",\"$sales_region\",\"$treg\",\"$connected_router\")";
	
		$insert_row = "INSERT INTO t_report_ippm_inp_weekly 
		(`weeknum`,`date_id`,`reg_name`,`rnc_name`,`ani`,`vendor`,`site_id`,`site_name`,`transport`,`transport_type`,`max_pl`,`counter_max`,`avg_pl`,`counter_avg`,`category_max35`,`category_avg21`,`distribution_max`,`distribution_avg`,`count_error_pl`,`count_lat`,`latency`,`max_latency`,`category_latency`,`distribution_latency`,`jitter`,`category_jitter`,`distribution_jitter`,`count_iub_drop`,`count_iub_drop01`,`iub_drop`,`category_iub_drop`,`category_iub_drop01`,`count_crc`,`crc`,`category_crc`,`count_discards`,`discards`,`category_discards`,`vlan_id`,`kabupaten`,`rtpo`,`cluster_name`,`trueconex_cluster`,`config_3g`,`count_availability`,`availability_status`,`sales_region`,`treg`,`connected_router`) 
		values $list_row;";  
		
		$pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
		}
	}
	}

} 
?>