<?php 

function AO_AA01_REF_ANI_main() { 

	global $G_DBCONN_MAIN; 
	     
        //server sumber 
        $server_sumber      = "10.54.18.122:3306";
        $username_sumber    = "transport"; 
        $password_sumber    = "Transport#2019"; 
        $database_sumber    = "TNQ";
        $conn_sumber        = mysqli_connect($server_sumber, $username_sumber, $password_sumber, $database_sumber);

        //proses get data
        $get_data_query = "SELECT * FROM REF_ANI WHERE weeknum = (SELECT max(weeknum) from REF_ANI) LIMIT 1"; 
        $result = $conn_sumber->query($get_data_query);             
        
        if(!empty($result)){  
 
                //delete week yang sudah melewati 1 bulan terakhir
                $delete_data_query = "DELETE FROM t_ref_ani WHERE weeknum <= (SELECT max(weeknum)-4 from t_ref_ani)";   
                $result_delete = $G_DBCONN_MAIN->query($delete_data_query); 

                $i = 0; 
                while($fault = mysqli_fetch_array($result))
                { 

                $regional						= str_replace(array('"'), '', $fault['regional']); 
                $reg_name						= str_replace(array('"'), '', $fault['reg_name']);
                $rnc_ani				    	= str_replace(array('"'), '', $fault['rnc_ani']); 
                $rnc_name	                    = str_replace(array('"'), '', $fault['rnc_name']);
                $ani	                        = str_replace(array('"'), '', $fault['ani']); 
                $site_name					    = str_replace(array('"'), '', $fault['site_name']);  
                $site_name2						= str_replace(array('"'), '', $fault['site_name2']); 
                $node_id						= str_replace(array('"'), '', $fault['node_id']);
                $transport						= str_replace(array('"'), '', $fault['transport']);
                $transport_type					= str_replace(array('"'), '', $fault['transport_type']);
                $transport_mode					= str_replace(array('"'), '', $fault['transport_mode']);
                $bw_ordered				    	= str_replace(array('"'), '', $fault['bw_ordered']);
                $vlan_id						= str_replace(array('"'), '', $fault['vlan_id']);
                $longi					        = str_replace(array('"'), '', $fault['longi']);
                $lat					        = str_replace(array('"'), '', $fault['lat']);
                $link_route			    		= str_replace(array('"'), '', $fault['link_route']);
                $kabupaten			            = str_replace(array('"'), '', $fault['kabupaten']);
                $rtpo		            		= str_replace(array('"'), '', $fault['rtpo']); 
                $bw_actual			        	= str_replace(array('"'), '', $fault['bw_actual']);
                $sctp_link_id					= str_replace(array('"'), '', $fault['sctp_link_id']);
                $bbc_status						= str_replace(array('"'), '', $fault['bbc_status']);
                $bbc_cluster_2017				= str_replace(array('"'), '', $fault['bbc_cluster_2017']);
                $bbc_class_2019		    		= str_replace(array('"'), '', $fault['bbc_class_2019']);
                $kabupaten_truebex		    	= str_replace(array('"'), '', $fault['kabupaten_truebex']);
                $province					    = str_replace(array('"'), '', $fault['province']);
                $poi_category			    	= str_replace(array('"'), '', $fault['poi_category']);
                $poi_name		            	= str_replace(array('"'), '', $fault['poi_name']);
                $poi_type				    	= str_replace(array('"'), '', $fault['poi_type']); 
                $lac		    	            = str_replace(array('"'), '', $fault['lac']);
                $ci					            = str_replace(array('"'), '', $fault['ci']);
                $desa				            = str_replace(array('"'), '', $fault['desa']);
                $kecamatan		 	            = str_replace(array('"'), '', $fault['kecamatan']);
                $mac_address			        = str_replace(array('"'), '', $fault['mac_address']);
                $cluster_name		            = str_replace(array('"'), '', $fault['cluster_name']);
                $trueconex_cluster		        = str_replace(array('"'), '', $fault['trueconex_cluster']);
                $trueconex_scope		    	= str_replace(array('"'), '', $fault['trueconex_scope']);
                $config_3g				    	= str_replace(array('"'), '', $fault['config_3g']);
                $rnc_name2		    	        = str_replace(array('"'), '', $fault['rnc_name2']);
                $vendor				            = str_replace(array('"'), '', $fault['vendor']); 
                $treg		                	= str_replace(array('"'), '', $fault['treg']);
                $connected_router	   	        = str_replace(array('"'), '', $fault['connected_router']);
                $mgw_target			     	    = str_replace(array('"'), '', $fault['mgw_target']);
                $rnc_route		            	= str_replace(array('"'), '', $fault['rnc_route']);
                $rnc_mgw				    	= str_replace(array('"'), '', $fault['rnc_mgw']);
                $ipran_transport			    = str_replace(array('"'), '', $fault['ipran_transport']);
                $ipbb_transport	                = str_replace(array('"'), '', $fault['ipbb_transport']);
                $network_category		    	= str_replace(array('"'), '', $fault['network_category']); 
                $weeknum	                    = str_replace(array('"'), '', $fault['weeknum']);
                $metro   	                    = str_replace(array('"'), '', $fault['metro']);
                        
                $list_row	= "(\"$regional\",\"$reg_name\",\"$rnc_ani\",\"$rnc_name\",\"$ani\",\"$site_name\",\"$site_name2\",\"$node_id\",\"$transport\",\"$transport_type\",\"$transport_mode\",\"$bw_ordered\",\"$vlan_id\",\"$longi\",\"$lat\",\"$link_route\",\"$kabupaten\",\"$rtpo\",\"$bw_actual\",\"$sctp_link_id\",\"$bbc_status\",\"$bbc_cluster_2017\",\"$bbc_class_2019\",\"$kabupaten_truebex\",\"$province\",\"$poi_category\",\"$poi_name\",\"$poi_type\",\"$lac\",\"$ci\",\"$desa\",\"$kecamatan\",\"$mac_address\",\"$cluster_name\",\"$trueconex_cluster\",\"$trueconex_scope\",\"$config_3g\",\"$rnc_name2\",\"$vendor\",\"$treg\",\"$connected_router\",\"$mgw_target\",\"$rnc_route\",\"$rnc_mgw\",\"$ipran_transport\",\"$ipbb_transport\",\"$network_category\",\"$weeknum\",\"$metro\")"; 
                        
                if($i == 0) {  
                        //sebelum data di insert, hapus dulu data berdasarkan week yang di get, agar tidak double
                        //tidak pakai REPLACE dikarenakan narik apa adanya dari server, meminimalisir kesalahan penarikan data
                        $delete_data_query_this_week = "DELETE FROM t_ref_ani WHERE weeknum = $weeknum";  
                        $result_delete_this_week = $G_DBCONN_MAIN->query($delete_data_query_this_week);
                }

                $insert_row = "INSERT INTO t_ref_ani 
                (`regional`,`reg_name`,`rnc_ani`,`rnc_name`,`ani`,`site_name`,`site_name2`,`node_id`,`transport`,`transport_type`,`transport_mode`,`bw_ordered`,`vlan_id`,`longi`,`lat`,`link_route`,`kabupaten`,`rtpo`,`bw_actual`,`sctp_link_id`,`bbc_status`,`bbc_cluster_2017`,`bbc_class_2019`,`kabupaten_truebex`,`province`,`poi_category`,`poi_name`,`poi_type`,`lac`,`ci`,`desa`,`kecamatan`,`mac_address`,`cluster_name`,`trueconex_cluster`,`trueconex_scope`,`config_3g`,`rnc_name2`,`vendor`,`treg`,`connected_router`,`mgw_target`,`rnc_route`,`rnc_mgw`,`ipran_transport`,`ipbb_transport`,`network_category`,`weeknum`,`metro`) values $list_row;";  
                
                $pushh      = mysqli_query($G_DBCONN_MAIN,$insert_row) or die(mysqli_error($G_DBCONN_MAIN));
                } 
                $i++;
        }
 
} 
?>